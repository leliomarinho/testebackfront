package com.leliomarinho.provabackfront;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.leliomarinho.provabackfront.api.entity.Cliente;
import com.leliomarinho.provabackfront.api.entity.Itinerarios;
import com.leliomarinho.provabackfront.api.entity.Linha;
import com.leliomarinho.provabackfront.api.entity.User;
import com.leliomarinho.provabackfront.api.enuns.EnumTipo;
import com.leliomarinho.provabackfront.api.enuns.ProfileEnum;
import com.leliomarinho.provabackfront.api.repository.ClienteRepository;
import com.leliomarinho.provabackfront.api.repository.LinhaRepository;
import com.leliomarinho.provabackfront.api.repository.UserRepository;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

@SpringBootApplication
public class ProvaBackFrontApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvaBackFrontApplication.class, args);

	}

	@Bean
	CommandLineRunner init(UserRepository userRepository, PasswordEncoder passwordEncoder,
			LinhaRepository linharepository, ClienteRepository cliente) {
		return args -> {
			initUsers(userRepository, passwordEncoder);
			initLinhas(linharepository);
			initCliente(cliente);

		};

	}

	private void initUsers(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		User admin = new User();
		admin.setEmail("admin@teste.com");
		admin.setPassword(passwordEncoder.encode("123456"));
		admin.setProfile(ProfileEnum.ROLE_ADMIN);

		User find = userRepository.findByEmail("admin@teste.com");
		if (find == null) {
			userRepository.save(admin);
		}
	}

	private void initCliente(ClienteRepository cliente) {
		Cliente clientes = new Cliente();

		clientes.setCpf("111.111.111-11");
		clientes.setEmail("teste@teste.com.br");
		clientes.setNome("Lelio cadastro teste");

		List<Linha> linhas = new ArrayList<Linha>();

		Linha linha = new Linha();

		linha.setCodigoId("5c4653f1e1643837ccce9daf");

		linhas.add(linha);

		cliente.save(clientes);

	}

	private void initLinhas(LinhaRepository linharepository)
			throws JsonParseException, JsonMappingException, IOException {

		try {

			URL url = new URL("http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}

			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);

			String output;

			while ((output = br.readLine()) != null) {

				Gson gson = new Gson();

				Type type = new TypeToken<List<Linha>>() {
				}.getType();

				List<Linha> listsOnibus = gson.fromJson(output.replace("id", "codigoId"), type);

				for (Linha resultado : listsOnibus) {

					Linha cadastrolinhas = new Linha();
					cadastrolinhas.setCodigo(resultado.getCodigo());
					cadastrolinhas.setCodigoId(resultado.getCodigoId());
					cadastrolinhas.setNome(resultado.getNome());
					cadastrolinhas.setTipo(EnumTipo.O);

					Linha find = linharepository.findByCodigo(resultado.getCodigo());

					if (find == null) {
						linharepository.save(cadastrolinhas);
					}

				}

			}

			conn.disconnect();

			url = new URL("http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l");
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}

			in = new InputStreamReader(conn.getInputStream());
			BufferedReader buffer = new BufferedReader(in);

			while ((output = buffer.readLine()) != null) {

				Gson gson = new Gson();

				Type type = new TypeToken<List<Linha>>() {
				}.getType();

				List<Linha> listsOnibus = gson.fromJson(output.replace("id", "codigoId"), type);

				for (Linha resultado : listsOnibus) {

					Linha cadastrolinhas = new Linha();
					cadastrolinhas.setCodigo(resultado.getCodigo());
					cadastrolinhas.setCodigoId(resultado.getCodigoId());
					cadastrolinhas.setNome(resultado.getNome());
					cadastrolinhas.setTipo(EnumTipo.L);

					Linha find = linharepository.findByCodigo(resultado.getCodigo());

					if (find == null) {
						linharepository.save(cadastrolinhas);
					}

				}

			}
			conn.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		}

	}

}
