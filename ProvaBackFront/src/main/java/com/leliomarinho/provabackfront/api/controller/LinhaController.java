package com.leliomarinho.provabackfront.api.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.leliomarinho.provabackfront.api.entity.Itinerarios;
import com.leliomarinho.provabackfront.api.entity.Linha;
import com.leliomarinho.provabackfront.api.enuns.EnumTipo;
import com.leliomarinho.provabackfront.api.response.Response;
import com.leliomarinho.provabackfront.api.service.LinhaService;

@RestController
@RequestMapping(path = "/api/linha")
@CrossOrigin(origins = "*")
public class LinhaController {

	@Autowired
	private LinhaService linhaService;

	@Value("${linha.endpoint}")
	private String endpoint;

	private void validateTipo(Linha linha, BindingResult result) {
		if (linha.getTipo() == null) {

			result.addError(new ObjectError("Linhas", "Tipo não informado"));
			return;
		}
		if (linha.getTipo() != EnumTipo.O || linha.getTipo() != EnumTipo.L) {

			result.addError(new ObjectError("Linhas", "Tipo não inforencontrado"));
			return;
		}
	}

	@GetMapping(value = "itinerario/{codigo}")
	public ResponseEntity<Response<Linha>> findItinerario(@PathVariable("codigo") String codigo) {

		Response<Linha> response = new Response<Linha>();

		Linha linha = linhaService.findByCodigoId(codigo);

		if (linha == null) {

			response.getErrors().add("Cadasro não encontrado com o codigo: " + codigo);
			return ResponseEntity.badRequest().body(response);
		}

		try {
			
			System.out.println(endpoint + "?a=il&p=" + linha.getCodigoId());

			URL url = new URL(endpoint + "?a=il&p=" + linha.getCodigoId());

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}

			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);

			String jsonString;
			
			

			List<Itinerarios> changeItinerarios = new ArrayList<Itinerarios>();

			while ((jsonString = br.readLine()) != null) {


				JsonObject jobj = new Gson().fromJson(jsonString, JsonObject.class);

				ObjectMapper mapper = new ObjectMapper();

				JsonNode dados = mapper.readTree(jsonString);

				for (int i = 0; i < dados.size() - 3; i++) {

					Itinerarios changeItinerario = new Itinerarios();

					changeItinerario.setId(Integer.toString(i));

					JsonObject jo = jobj.getAsJsonObject().get(Integer.toString(i)).getAsJsonObject();

					final AtomicInteger contador = new AtomicInteger();

					jo.entrySet().stream().forEach(qm -> {

						String key = qm.getKey();

						contador.incrementAndGet();

						JsonElement je = qm.getValue();
						
						//System.out.println(key + ":" + je);

						if ("lat".equals(key)) {
							changeItinerario.setLat("-" + je.toString().replaceAll("[^0-9]", ""));

						}

						if ("lng".equals(key)) {

							changeItinerario.setLng("-" + je.toString().replaceAll("[^0-9]", ""));
						}

					});

					changeItinerarios.add(changeItinerario);

				}

			}
			conn.disconnect();

			linha.setItnerarios(changeItinerarios);

			response.setData(linha);

			return ResponseEntity.ok(response);

		} catch (

		Exception e) {

			response.getErrors().add("Exception in NetClientGet:- " + e);
			return ResponseEntity.badRequest().body(response);

		}

	}

	@PostMapping()
	public ResponseEntity<Response<Linha>> create(HttpServletRequest request, @RequestBody Linha linha,
			BindingResult result) {

		Response<Linha> response = new Response<Linha>();

		try {

			validateTipo(linha, result);
			if (result.hasErrors()) {

				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));

				return ResponseEntity.badRequest().body(response);

			}

			Linha linhaPersisted = (Linha) linhaService.createOrUpdate(linha);

			response.setData(linhaPersisted);

		} catch (DuplicateKeyException dE) {

			response.getErrors().add("Id já Cadastrado !");

			return ResponseEntity.badRequest().body(response);

		} catch (Exception e) {
			response.getErrors().add(e.getMessage());

			return ResponseEntity.badRequest().body(response);

		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "codigo/{codigo}")
	public ResponseEntity<Response<Linha>> findByCodigo(@PathVariable("codigo") String codigo) {

		Response<Linha> response = new Response<Linha>();

		Linha linha = linhaService.findByCodigo(codigo);

		if (linha == null) {

			response.getErrors().add("Cadasro não encontrado com o codigo: " + codigo);
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(linha);
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}/{tipo}/nome/{nome}")
	public ResponseEntity<Response<Linha>> findByNome(HttpServletRequest request, @PathVariable int page,
			@PathVariable int count, @PathVariable String tipo, @PathVariable String nome){

		Response<Linha> response = new Response<Linha>();

		Linha linha = linhaService.findByNomeIgnoreCaseContainingAndTipoIgnoreCaseContaining(page, count, nome, tipo );

		if (linha == null) {

			response.getErrors().add("Cadasro não encontrado com o nome: " + nome);
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(linha);
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "id/{id}")
	public ResponseEntity<Response<Linha>> findById(@PathVariable("id") String id) {

		Response<Linha> response = new Response<Linha>();

		Linha linha = linhaService.findById(id);

		if (linha == null) {

			response.getErrors().add("Cadasro não encontrado com o id:" + id);
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(linha);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "codigoid/{id}")
	public ResponseEntity<Response<Linha>> findCodigoId(@PathVariable("id") String id) {

		Response<Linha> response = new Response<Linha>();

		Linha linha = linhaService.findById(id);

		if (linha == null) {

			response.getErrors().add("Cadasro não encontrado com o id:" + id);
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(linha);
		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Response<String>> delete(@PathVariable("id") String id) {

		Response<String> response = new Response<String>();

		Linha linha = linhaService.findById(id);

		if (linha == null) {
			response.getErrors().add("Cadasro não encontrado com o id:" + id);
			return ResponseEntity.badRequest().body(response);
		}
		linhaService.delete(id);
		return ResponseEntity.ok(new Response<String>());
	}

	@GetMapping(value = "{page}/{count}")
	public ResponseEntity<Response<Page<Linha>>> findAll(@PathVariable int page, @PathVariable int count) {

		Response<Page<Linha>> response = new Response<Page<Linha>>();

		Page<Linha> linhas = linhaService.findAll(page, count);

		response.setData(linhas);

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}/{tipo}")
	public ResponseEntity<Response<Page<Linha>>> findByParams(HttpServletRequest request, @PathVariable int page,
			@PathVariable int count, @PathVariable String tipo) {

		Response<Page<Linha>> response = new Response<Page<Linha>>();
		Page<Linha> linhasTipo = null;

		linhasTipo = linhaService.findByTipo(page, count, tipo);

		response.setData(linhasTipo);
		return ResponseEntity.ok(response);
	}

}
