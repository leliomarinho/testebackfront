package com.leliomarinho.provabackfront.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.leliomarinho.provabackfront.api.entity.Linha;
import com.leliomarinho.provabackfront.api.repository.LinhaRepository;
import com.leliomarinho.provabackfront.api.service.LinhaService;

@Service
public class LinhaServiceImpl implements LinhaService {

	@Autowired
	private LinhaRepository linharepository;
	
	
	public Linha createOrUpdate(Linha linha) {

		return this.linharepository.save(linha);
	}

	@Override
	public Linha findById(String id) {

		return this.linharepository.findOne(id);

	}

	@Override
	public void delete(String id) {

		this.linharepository.delete(id);

	}

	@Override
	public Linha findByCodigo(String codigo) {

		return this.linharepository.findByCodigo(codigo);

	}
	
	public Page<Linha> findByTipo(int page, int count,String tipo){
		Pageable pages = new PageRequest(page, count);
		return this.linharepository.findByTipo(tipo, pages);
	}
	
	@Override
	public Page<Linha> findAll(int page, int count) {

		Pageable pages = new PageRequest(page, count);
		return linharepository.findAll(pages);

	}

	@Override
	public Linha findByNomeIgnoreCaseContainingAndTipoIgnoreCaseContaining(int page, int count,String nome, String tipo) {
		
		return this.linharepository.findByNomeIgnoreCaseContainingAndTipoIgnoreCaseContaining(page, count, nome, tipo);
	}

	@Override
	public Linha findByCodigoId(String id) {
		
		return this.linharepository.findByCodigoId(id);
	}
	

}
