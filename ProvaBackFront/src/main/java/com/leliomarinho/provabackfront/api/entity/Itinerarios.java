package com.leliomarinho.provabackfront.api.entity;

import org.springframework.data.annotation.Id;

public class Itinerarios {
	
	@Id
	private String id;
	
	private String lat;
	
	private String lng;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	

	
	

}
