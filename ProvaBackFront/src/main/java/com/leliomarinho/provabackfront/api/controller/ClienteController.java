package com.leliomarinho.provabackfront.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.leliomarinho.provabackfront.api.entity.Cliente;
import com.leliomarinho.provabackfront.api.entity.Linha;
import com.leliomarinho.provabackfront.api.response.Response;
import com.leliomarinho.provabackfront.api.service.ClienteService;

@RestController
@RequestMapping(path = "/api/cliente")
@CrossOrigin(origins = "*")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@PostMapping()
	public ResponseEntity<Response<Cliente>> create(HttpServletRequest request, @RequestBody Cliente cliente,
			BindingResult result) {

		Response<Cliente> response = new Response<Cliente>();

		try {

			validateCreatecliente(cliente, result);
			if (result.hasErrors()) {

				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));

				return ResponseEntity.badRequest().body(response);

			}

			Cliente clientePersisted = (Cliente) clienteService.createOrUpdate(cliente);

			response.setData(clientePersisted);

		} catch (DuplicateKeyException dE) {

			response.getErrors().add("CPF já Cadastrado !");

			return ResponseEntity.badRequest().body(response);

		} catch (Exception e) {
			response.getErrors().add(e.getMessage());

			return ResponseEntity.badRequest().body(response);

		}
		return ResponseEntity.ok(response);
	}

	private void validateCreatecliente(Cliente cliente, BindingResult result) {
		if (cliente.getCpf() == null) {

			result.addError(new ObjectError("Cliente", "CPF não informado"));

			return;
		}
	}

	@PutMapping()
	public ResponseEntity<Response<Cliente>> update(HttpServletRequest request, @RequestBody Cliente cliente,

			BindingResult result) {

		Response<Cliente> response = new Response<Cliente>();

		try {
			validateUpdate(cliente, result);
			if (result.hasErrors()) {

				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));

				return ResponseEntity.badRequest().body(response);
			}

			Cliente clientePersisted = (Cliente) clienteService.createOrUpdate(cliente);

			response.setData(clientePersisted);

		} catch (Exception e) {

			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	private void validateUpdate(Cliente cliente, BindingResult result) {
		if (cliente.getId() == null) {

			result.addError(new ObjectError("Cliente", "Id não informado"));
			return;
		}
		if (cliente.getCpf() == null) {

			result.addError(new ObjectError("Cliente", "CPF não informado"));
			return;
		}
	}

	@GetMapping(value = "{id}")
	public ResponseEntity<Response<Cliente>> findById(@PathVariable("id") String id) {

		Response<Cliente> response = new Response<Cliente>();

		Cliente cliente = clienteService.findById(id);

		if (cliente == null) {

			response.getErrors().add("Cadastro não encontrado com o id:" + id);
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(cliente);
		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Response<String>> delete(@PathVariable("id") String id) {

		Response<String> response = new Response<String>();

		Cliente cliente = clienteService.findById(id);

		if (cliente == null) {
			response.getErrors().add("Cadastro não encontrado com o id:" + id);
			return ResponseEntity.badRequest().body(response);
		}
		clienteService.delete(id);
		return ResponseEntity.ok(new Response<String>());
	}
	
	@GetMapping(value = "{page}/{count}/nome/{nome}")
	public ResponseEntity<Response<Page<Cliente>>> findByParams(HttpServletRequest request, @PathVariable int page,
			@PathVariable int count, @PathVariable String nome) {

		Response<Page<Cliente>> response = new Response<Page<Cliente>>();
		Page<Cliente> clientesNome = null;

		clientesNome = clienteService.findByNome(page, count, nome);

		response.setData(clientesNome);
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}")
	public ResponseEntity<Response<Page<Cliente>>> findAll(@PathVariable int page, @PathVariable int count) {

		Response<Page<Cliente>> response = new Response<Page<Cliente>>();

		Page<Cliente> clientes = clienteService.findAll(page, count);

		response.setData(clientes);

		return ResponseEntity.ok(response);
	}
}