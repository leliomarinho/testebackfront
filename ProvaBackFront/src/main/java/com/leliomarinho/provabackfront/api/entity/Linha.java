package com.leliomarinho.provabackfront.api.entity;


import java.util.List;

import org.springframework.data.annotation.Id;

import com.leliomarinho.provabackfront.api.enuns.EnumTipo;


public class Linha {
	
	@Id
	private String id;

	private String codigoId;
	
	private String codigo;
	
	private String nome;
	
	private EnumTipo tipo;	
	
	private List<Itinerarios> itnerarios;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getCodigoId() {
		return codigoId;
	}


	public void setCodigoId(String codigoId) {
		this.codigoId = codigoId;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	

	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public EnumTipo getTipo() {
		return tipo;
	}


	public void setTipo(EnumTipo tipo) {
		this.tipo = tipo;
	}


	public List<Itinerarios> getItnerarios() {
		return itnerarios;
	}


	public void setItnerarios(List<Itinerarios> itnerarios) {
		this.itnerarios = itnerarios;
	}


}
