package com.leliomarinho.provabackfront.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.leliomarinho.provabackfront.api.entity.User;


public interface UserRepository extends MongoRepository<User, String> {

	User findByEmail(String email);

}