package com.leliomarinho.provabackfront.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.leliomarinho.provabackfront.api.entity.Cliente;


public interface ClienteRepository extends MongoRepository<Cliente, String> {
	
	Page<Cliente> findByNomeIgnoreCaseContaining(String tipo,Pageable pages);

}