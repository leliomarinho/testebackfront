package com.leliomarinho.provabackfront.api.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.leliomarinho.provabackfront.api.entity.Linha;


public interface LinhaRepository extends MongoRepository<Linha, String> {

	Linha findByCodigo (String codigo);
	
	Linha findByCodigoId (String id);

	Linha findByNomeIgnoreCaseContainingAndTipoIgnoreCaseContaining (int page, int count,String nome, String tipo);
	
	Page<Linha> findByTipo(String tipo,Pageable pages);


}
