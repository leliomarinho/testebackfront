package com.leliomarinho.provabackfront.api.service;

import org.springframework.data.domain.Page;

import com.leliomarinho.provabackfront.api.entity.Cliente;

public interface ClienteService {
	
	Cliente createOrUpdate(Cliente cliente);
	
	Cliente findById(String id);
	
	Page<Cliente> findByNome(int page, int count,String nome);
	
	void delete(String id);
	
	Page<Cliente> findAll(int page, int count);
}