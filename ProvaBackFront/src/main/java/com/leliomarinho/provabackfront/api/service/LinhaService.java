package com.leliomarinho.provabackfront.api.service;


import org.springframework.data.domain.Page;

import com.leliomarinho.provabackfront.api.entity.Linha;


public interface LinhaService {
	
	Linha createOrUpdate(Linha linha);
	
	Linha findById(String id);
	
	void delete(String id);
	
	Page<Linha> findAll(int page, int count);
	
	Linha findByCodigo (String codigo);
	
	Linha findByCodigoId (String id);
	
	Linha findByNomeIgnoreCaseContainingAndTipoIgnoreCaseContaining (int page, int count,String nome, String tipo);
	
	Page<Linha> findByTipo(int page, int count,String tipo);
	
	
	
}
