package com.leliomarinho.provabackfront.api.enuns;

public enum ProfileEnum {
	ROLE_ADMIN,
	ROLE_CUSTOMER,
	ROLE_TECHNICIAN
}