package com.leliomarinho.provabackfront.api.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.leliomarinho.provabackfront.api.entity.Cliente;
import com.leliomarinho.provabackfront.api.repository.ClienteRepository;
import com.leliomarinho.provabackfront.api.service.ClienteService;


@Service
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteRepository clienteRespository;


	public Cliente createOrUpdate(Cliente Cliente) {
		
		return this.clienteRespository.save(Cliente);
	}
	
	@Override
	public Cliente findById(String id) {
		
		return this.clienteRespository.findOne(id);
		
	}

	
	@Override
	public void delete(String id) {
		this.clienteRespository.delete(id);
		
	}

	
	@Override
	public Page<Cliente> findAll(int page, int count) {
		
		Pageable pages= new PageRequest(page,count);
		return clienteRespository.findAll(pages);
		
	}
	
	public Page<Cliente> findByNome(int page, int count,String tipo){
		
		Pageable pages = new PageRequest(page, count);
		
		return this.clienteRespository.findByNomeIgnoreCaseContaining(tipo, pages);
	}



}