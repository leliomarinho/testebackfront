Boa noite, Matheus!

Conforme solicitado, segue a prova:

# Prova Back e Front 

### Observações:

Efetuei uma poc para testar o mapa, mas quando fui integrar com o meu projeto não funcionou por conta de versão, estou a algum tempo tentando corrigir sem sucesso, com isso a prova nao ficou 100% concluída, mas coloquei a poc para verificar a lógica e as tecnologias utilizadas

## Pasta Prova BackEnd:

## ProvaBackFront

## Pasta Prova FrontEnd:

 * ProvaFront

 * Pasta POC:

ProvaMapa

todos os projetos estão no gitlab:

[git-repo-url] https://gitlab.com/leliomarinho/testebackfront

# Tecnologias utilizadas:
## BackEnd

### Framworks:

 * Spring Boot
 * Spring Security
 * Spring Data

### Banco de Dados
 
* MongoDB

## FrontEnd

### Framworks:

 * Angular
 * BootStrap

 * Usuario padrão para acessar o sistema

 ```json
    email: admin@teste.com
    senha: 123456 

 ```

### Banco de dados
 
* Apontamento para a API BackEnd porta 8080

Arquivo de Configuraçao: application.properties
 ```json

 // IP de Configuração do banco
 spring.data.mongodb.host=localhost
 // Porta de Configuração do banco
spring.data.mongodb.port=27017
// Nome do Banco
spring.data.mongodb.database=ProvaBackFront

// Secret 
jwt.secret=provafront_klay
# espirar em 7 dias
jwt.expiration=604800
// endpont para acessar as Linhas e Itinerarios
linha.endpoint=http://www.poatransporte.com.br/php/facades/process.php


 ```


# Principais EndPoints:

## Recurso Autenticação

Post: http://localhost:8080/api/auth
```json
Request:
{
	"email" : "admin@teste.com",
	"password" : "123456"
	
}
Response:
{
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkB0ZXN0ZS5jb20iLCJjcmVhdGVkIjoxNTQ4MTkzMjE4Mjk2LCJleHAiOjE1NDg3OTgwMTh9.Vb2yfhI0c60RQ6LftPVNB7_Z7aDTpEM6oszVfGp7KAcEn7KD0LP54qyV8Y3hFI1_fZM5NiN569YKQHDRXgx3TA",
    "user": {
        "id": "5c4786f1e1643838567d5c89",
        "email": "admin@teste.com",
        "password": null,
        "profile": "ROLE_ADMIN"
    }
}
```


## Recurso Cliente

Post : http://localhost:8080/api/usuario
```json
Request:

{
	"nome" : "Lelio Marinho",
	"email" : "leliomarinho@gmail.com",
	"cpf" : "111.111.111-11",
	"linha" : [{ "id" : "5c447ee4e16438194e5f9a46" }]
}

Response:

{
    "data": {
        "id": "5c47b5e2e1643838567d5c8b",
        "nome": "Lelio Marinho",
        "email": "leliomarinho@gmail.com",
        "cpf": "11.111.111-11",
        "linha": [
            {
                "id": "5c4653f1e1643837ccce9daf",
                "codigoId": null,
                "codigo": null,
                "nome": null,
                "tipo": null,
                "itnerarios": null
            },
            {
                "id": "5c4653f1e1643837ccce9db7",
                "codigoId": null,
                "codigo": null,
                "nome": null,
                "tipo": null,
                "itnerarios": null
            },
            {
                "id": "5c4653ede1643837ccce99db",
                "codigoId": null,
                "codigo": null,
                "nome": null,
                "tipo": null,
                "itnerarios": null
            }
        ]
    },
    "errors": []
}

```

Put : http://localhost:8080/api/usuario

```json
Request:

{
	"nome" : "Lelio Marinho",
	"email" : "leliomarinho@gmail.com",
	"cpf" : "111.111.111-11",
	"linha" : [{ "id" : "5c447ee4e16438194e5f9a46" }]
}

Response:

{
    "data": {
        "id": "5c47b5e2e1643838567d5c8b",
        "nome": "Lelio Marinho",
        "email": "leliomarinho@gmail.com",
        "cpf": "11.111.111-11",
        "linha": [
            {
                "id": "5c4653f1e1643837ccce9daf",
                "codigoId": null,
                "codigo": null,
                "nome": null,
                "tipo": null,
                "itnerarios": null
            },
            {
                "id": "5c4653f1e1643837ccce9db7",
                "codigoId": null,
                "codigo": null,
                "nome": null,
                "tipo": null,
                "itnerarios": null
            },
            {
                "id": "5c4653ede1643837ccce99db",
                "codigoId": null,
                "codigo": null,
                "nome": null,
                "tipo": null,
                "itnerarios": null
            }
        ]
    },
    "errors": []
}

```

Delete : http://localhost:8080/api/usuario

```json
Request:

{
	"nome" : "Lelio Marinho",
	"email" : "leliomarinho@gmail.com",
	"cpf" : "111.111.111-11",
	"linha" : [{ "id" : "5c447ee4e16438194e5f9a46" }]
}

Response:

{
    "data": {
        "id": "5c47b5e2e1643838567d5c8b",
        "nome": "Lelio Marinho",
        "email": "leliomarinho@gmail.com",
        "cpf": "11.111.111-11",
        "linha": [
            {
                "id": "5c4653f1e1643837ccce9daf",
                "codigoId": null,
                "codigo": null,
                "nome": null,
                "tipo": null,
                "itnerarios": null
            },
            {
                "id": "5c4653f1e1643837ccce9db7",
                "codigoId": null,
                "codigo": null,
                "nome": null,
                "tipo": null,
                "itnerarios": null
            },
            {
                "id": "5c4653ede1643837ccce99db",
                "codigoId": null,
                "codigo": null,
                "nome": null,
                "tipo": null,
                "itnerarios": null
            }
        ]
    },
    "errors": []
}

```

GET : http://localhost:8080/api/usuario/{id}


```json
Response:

{
    "data": {
        "id": "5c47b5e2e1643838567d5c8b",
        "nome": "Lelio Marinho",
        "email": "leliomarinho@gmail.com",
        "cpf": "11.111.111-11",
        "linha": [     ]
    },
    "errors": []
}

```


GET : http://localhost:8080/api/usuario/{page}/{count}
* Get com Paginação 


```json

Response:

{
    "data": {
        "content": [
            {
                "id": "5c48f737e1643845615f000a",
                "nome": "Lelio cadastro teste",
                "email": "teste@teste.com.br",
                "cpf": "111.111.111-11",
                "linha": null
            },
            {
                "id": "5c48f913e1643845615f000b",
                "nome": "Lelio Marinho",
                "email": "leliomarinho@gmail.com",
                "cpf": "110.914.067-31",
                "linha": [
                    null,
                    null,
                    null
                ]
            }
        ],
        "last": true,
        "totalPages": 1,
        "totalElements": 2,
        "size": 10,
        "number": 0,
        "sort": null,
        "first": true,
        "numberOfElements": 2
    },
    "errors": []
}

```

GET : http://localhost:8080/api/usuario/{page}/{count}/nome/{nome}
* Get com Paginação e busca por nome


```json

Response:

{
    "data": {
        "content": [
            {
                "id": "5c48f737e1643845615f000a",
                "nome": "Lelio cadastro teste",
                "email": "teste@teste.com.br",
                "cpf": "111.111.111-11",
                "linha": null
            },
            {
                "id": "5c48f913e1643845615f000b",
                "nome": "Lelio Marinho",
                "email": "leliomarinho@gmail.com",
                "cpf": "110.914.067-31",
                "linha": [
                    null,
                    null,
                    null
                ]
            }
        ],
        "last": true,
        "totalPages": 1,
        "totalElements": 2,
        "size": 10,
        "number": 0,
        "sort": null,
        "first": true,
        "numberOfElements": 2
    },
    "errors": []
}

```
## Recurso User

* Post: http://localhost:8080/api/user
* Put : http://localhost:8080/api/user
* Delete: http://localhost:8080/api/user/{id}
* GET: http://localhost:8080/api/user/{id}
* GET: http://localhost:8080/api/user/{page}/{count}


## Recurso Linhas

* Post: http://localhost:8080/api/linha
* Put : http://localhost:8080/api/linha
* Delete: http://localhost:8080/api/linha/{id}
* GET: http://localhost:8080/api/linha/{id}
* GET: http://localhost:8080/api/linha/{page}/{count}
* GET: http://localhost:8080/api/itinerario/{page}/{count}
* GET: http://localhost:8080/api/codigo/{id}
* GET: http://localhost:8080/api/{page}/{count}/{tipo}/nome/{nome}
* GET: http://localhost:8080/api/codigo/{page}/{count}/{tipo}
** tipo: 'O' - Onibus 'L' - Lotação







