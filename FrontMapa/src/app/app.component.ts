import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { } from '@types/googlemaps';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  listaLinhas: any;
  tipo: string;
  codigolinha: string;
  url: string;
  itinerario: any;
  itinerarioAuxiliar: any;
  cor: string;

  urlOnibus = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o';
  urlLotacao = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=L';
  urlItinerarios = 'http://www.poatransporte.com.br/php/facades/process.php?a=il&p=';


  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  isHidden = false;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.IniciarMapa();

  }



  getRestLinhas(tipo: string): void {
    this.tipo = tipo;
    this.BuscarLinhas()
      .subscribe(
      restItems => {
        this.listaLinhas = restItems;
      }
      )
  }

  BuscarLinhas() {
    if (this.tipo == "O") {
      this.url = this.urlOnibus;
    }
    if (this.tipo == "L") {
      this.url = this.urlLotacao;
    }

    return this.http
      .get<any[]>(this.url)
      .pipe(map(data => data));

  }

  BuscarItinerarios() {
    return this.http
      .get<any[]>(this.urlItinerarios + this.codigolinha)
      .pipe(map(data => data));

  }

  getRestItinerarios() {
      this.BuscarItinerarios()
        .subscribe(
        restItems => {
          this.itinerario = restItems;
        }
      )
     this.DesenharMapa();
  }

  LimparMapa() {

    let mapProp = {

      center: new google.maps.LatLng(-30.03155681213800000, -51.22697925584400000),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP

    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

  }


  IniciarMapa() {

    let mapProp = {

      center: new google.maps.LatLng(-30.03155681213800000, -51.22697925584400000),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP

    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

  }


  
  AlterarTipoMapa(mapaTipoId: string) {
    this.map.setMapTypeId(mapaTipoId)
  }

  DesenharMapa() {

    // setTimeout(this.getRestItinerarios(), 20000);

     var fieldName1;
    // var fieldValue1 = 'value1';
    // var fieldName2 = 'name2';
    // var fieldValue2 = 'value2';

    // var object = {};
    // object[fieldName1] = fieldValue1;
    // object[fieldName2] = fieldValue2;

    //console.log(object);

    var lat = [];
    var lng = [];


    var jsObj = {};

    for (var i = 0; i < this.itinerario.length; i++) {
      jsObj['lat:' + i] = this.itinerario[i].lat;
      jsObj['lng:' + i] = this.itinerario[i].lng;
    }
    console.log(jsObj);



    for (var i = 0; i < this.itinerario.length; i++) {

//      lat[i] = this.itinerarioAuxiliar.lat
//      lng[i] = this.itinerarioAuxiliar.lng

    }

    //console.log(lat[0]);
    //console.log(lng[0]);


    lat[0] = ['-30.03155681213800000'];
    lng[0] = ['-51.22697925584400000'];
    lat[1] = ['-30.03154681213800000'];
    lng[1] = ['-51.22779525584400000'];
    lat[2] = ['-30.03075381213800000'];
    lng[2] = ['-51.22776425584400000'];
    lat[3] = ['-30.03077881213800000'];
    lng[3] = ['-51.22762825584400000'];
    lat[4] = ['-30.03083581213800000'];
    lng[4] = ['-51.22691325584400000'];
    lat[5] = ['-30.03090681213800000'];
    lng[5] = ['-51.22586725584400000'];
    lat[6] = ['-30.03097981213800000'];
    lng[6] = ['-51.22481125584400000'];
    lat[7] = ['-30.03102481213800000'];
    lng[7] = ['-51.22435225584400000'];
    lat[8] = ['-30.03106281213800000'];
    lng[8] = ['-51.22425625584400000'];
    lat[9] = ['-30.03112081213800000'];
    lng[9] = ['-51.22418925584400000'];
    lat[10] = ['-30.03146181213800000'];
    lng[10] = ['-51.22392425584400000'];
    lat[11] = ['-30.03191181213800000'];
    lng[11] = ['-51.22357725584400000'];
    lat[12] = ['-30.03218281213800000'];
    lng[12] = ['-51.22337725584400000'];
    lat[13] = ['-30.03249281213800000'];
    lng[13] = ['-51.22313525584400000'];
    lat[14] = ['-30.03251781213800000'];
    lng[14] = ['-51.22311625584400000'];
    lat[15] = ['-30.03346281213800000'];
    lng[15] = ['-51.22237825584400000'];
    lat[16] = ['-30.03354581213800000'];
    lng[16] = ['-51.22249925584400000'];
    lat[17] = ['-30.03411081213800000'];
    lng[17] = ['-51.22312025584400000'];
    lat[18] = ['-30.03418181213800000'];
    lng[18] = ['-51.22319425584400000'];
    lat[19] = ['-30.03461381213800000'];
    lng[19] = ['-51.22365225584400000'];
    lat[20] = ['-30.03466781213800000'];
    lng[20] = ['-51.22372125584400000'];
    lat[21] = ['-30.03463481213800000'];
    lng[21] = ['-51.22375925584400000'];



      for (var i = 0; i < this.itinerario.length; i++) {

        

        
//        console.log(this.itinerarioAuxiliar.lat);
//        console.log(this.itinerarioAuxiliar.lng);

        //lat[i] = this.itinerarioAuxiliar.lat
        //lng[i] = this.itinerarioAuxiliar.lng

      }



    var IniciarMapa = {
      center: new google.maps.LatLng(lat[0], lng[0]),
      zoom: 15,
      MapTypeId: google.maps.MapTypeId.ROADMAP,
      title: this.codigolinha

    };

    var novoMapa = new google.maps.Map(this.gmapElement.nativeElement, IniciarMapa)

    var latLngBounds = new google.maps.LatLngBounds();

    var RotaDestino = [];

    for (var i = 0; i < lat.length; i++) {

      RotaDestino.push(new google.maps.LatLng(lat[i], lng[i]));

    }

    if (this.tipo == "O") {
      this.cor = "#FF5563";
    }
    if (this.tipo == "L") {
      this.cor = "#000080";
    }

    var VisualizacaoMapa = {
      path: RotaDestino,
      strokeColor: this.cor,
      strokeWeight: 4
    };

    var polyline = new google.maps.Polyline(VisualizacaoMapa);

    polyline.setMap(novoMapa);

  }


}
