"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var http_1 = require("@angular/common/http");
var operators_1 = require("rxjs/operators");
var AppComponent = /** @class */ (function () {
    function AppComponent(http) {
        this.http = http;
        this.urlOnibus = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o';
        this.urlLotacao = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=L';
        this.urlItinerarios = 'http://www.poatransporte.com.br/php/facades/process.php?a=il&p=';
        this.isHidden = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.IniciarMapa();
    };
    AppComponent.prototype.getRestLinhas = function (tipo) {
        var _this = this;
        this.tipo = tipo;
        this.BuscarLinhas()
            .subscribe(function (restItems) {
            _this.listaLinhas = restItems;
        });
    };
    AppComponent.prototype.BuscarLinhas = function () {
        if (this.tipo == "O") {
            this.url = this.urlOnibus;
        }
        if (this.tipo == "L") {
            this.url = this.urlLotacao;
        }
        return this.http
            .get(this.url)
            .pipe(operators_1.map(function (data) { return data; }));
    };
    AppComponent.prototype.BuscarItinerarios = function () {
        return this.http
            .get(this.urlItinerarios + this.codigolinha)
            .pipe(operators_1.map(function (data) { return data; }));
    };
    AppComponent.prototype.getRestItinerarios = function () {
        var _this = this;
        this.BuscarItinerarios()
            .subscribe(function (restItems) {
            _this.itinerario = restItems;
        });
    };
    AppComponent.prototype.LimparMapa = function () {
        var mapProp = {
            center: new google.maps.LatLng(-30.03155681213800000, -51.22697925584400000),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    };
    AppComponent.prototype.IniciarMapa = function () {
        var mapProp = {
            center: new google.maps.LatLng(-30.03155681213800000, -51.22697925584400000),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    };
    AppComponent.prototype.AlterarTipoMapa = function (mapaTipoId) {
        this.map.setMapTypeId(mapaTipoId);
    };
    AppComponent.prototype.DesenharMapa = function () {
        console.log(this.codigolinha);
        this.getRestItinerarios();
        console.log(this.itinerario);
        console.log(this.urlItinerarios + this.codigolinha);
        var lat = [];
        var lng = [];
        // lat[0] = ['-30.03155681213800000'];
        // lng[0] = ['-51.22697925584400000'];
        // lat[1] = ['-30.03154681213800000'];
        // lng[1] = ['-51.22779525584400000'];
        // lat[2] = ['-30.03075381213800000'];
        // lng[2] = ['-51.22776425584400000'];
        // lat[3] = ['-30.03077881213800000'];
        // lng[3] = ['-51.22762825584400000'];
        // lat[4] = ['-30.03083581213800000'];
        // lng[4] = ['-51.22691325584400000'];
        // lat[5] = ['-30.03090681213800000'];
        // lng[5] = ['-51.22586725584400000'];
        // lat[6] = ['-30.03097981213800000'];
        // lng[6] = ['-51.22481125584400000'];
        // lat[7] = ['-30.03102481213800000'];
        // lng[7] = ['-51.22435225584400000'];
        // lat[8] = ['-30.03106281213800000'];
        // lng[8] = ['-51.22425625584400000'];
        // lat[9] = ['-30.03112081213800000'];
        // lng[9] = ['-51.22418925584400000'];
        // lat[10] = ['-30.03146181213800000'];
        // lng[10] = ['-51.22392425584400000'];
        // lat[11] = ['-30.03191181213800000'];
        // lng[11] = ['-51.22357725584400000'];
        // lat[12] = ['-30.03218281213800000'];
        // lng[12] = ['-51.22337725584400000'];
        // lat[13] = ['-30.03249281213800000'];
        // lng[13] = ['-51.22313525584400000'];
        // lat[14] = ['-30.03251781213800000'];
        // lng[14] = ['-51.22311625584400000'];
        // lat[15] = ['-30.03346281213800000'];
        // lng[15] = ['-51.22237825584400000'];
        // lat[16] = ['-30.03354581213800000'];
        // lng[16] = ['-51.22249925584400000'];
        // lat[17] = ['-30.03411081213800000'];
        // lng[17] = ['-51.22312025584400000'];
        // lat[18] = ['-30.03418181213800000'];
        // lng[18] = ['-51.22319425584400000'];
        // lat[19] = ['-30.03461381213800000'];
        // lng[19] = ['-51.22365225584400000'];
        // lat[20] = ['-30.03466781213800000'];
        // lng[20] = ['-51.22372125584400000'];
        // lat[21] = ['-30.03463481213800000'];
        // lng[21] = ['-51.22375925584400000'];
        for (var i = 0; i < this.itinerario.length; i++) {
            lat[i] = this.itinerario[i].lat;
            lng[i] = this.itinerario[i].lng;
        }
        var IniciarMapa = {
            center: new google.maps.LatLng(lat[0], lng[0]),
            zoom: 15,
            MapTypeId: google.maps.MapTypeId.ROADMAP,
            title: this.codigolinha
        };
        var novoMapa = new google.maps.Map(this.gmapElement.nativeElement, IniciarMapa);
        var latLngBounds = new google.maps.LatLngBounds();
        var RotaDestino = [];
        for (var i = 0; i < lat.length; i++) {
            RotaDestino.push(new google.maps.LatLng(lat[i], lng[i]));
        }
        if (this.tipo == "O") {
            this.cor = "#FF5563";
        }
        if (this.tipo == "L") {
            this.cor = "#000080";
        }
        var VisualizacaoMapa = {
            path: RotaDestino,
            strokeColor: this.cor,
            strokeWeight: 4
        };
        var polyline = new google.maps.Polyline(VisualizacaoMapa);
        polyline.setMap(novoMapa);
    };
    __decorate([
        core_2.ViewChild('gmap'),
        __metadata("design:type", Object)
    ], AppComponent.prototype, "gmapElement", void 0);
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map