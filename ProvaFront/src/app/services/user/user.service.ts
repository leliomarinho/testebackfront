import { User } from './../../model/user';
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { API_BACKEND } from '../provafront.api';

@Injectable()
export class UserService {
  
  constructor(private http: HttpClient) {}

  login(user: User){
    return this.http.post(`${API_BACKEND}/api/auth`,user);
  }

  createOrUpdate(user: User){
    if(user.id != null && user.id != ''){
      return this.http.put(`${API_BACKEND}/api/user`,user);
    } else {
      user.id = null;
      return this.http.post(`${API_BACKEND}/api/user`, user);
    }
  }

  findAll(page:number,count:number){
    return this.http.get(`${API_BACKEND}/api/user/${page}/${count}`);
  }

  findById(id:string){
    return this.http.get(`${API_BACKEND}/api/user/${id}`);
  }

  delete(id:string){
    return this.http.delete(`${API_BACKEND}/api/user/${id}`);
  }
}
