import { API_BACKEND } from './../provafront.api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cliente } from './../../model/cliente';


@Injectable()
export class ClienteService {
  constructor(private http: HttpClient) { }

  createOrUpdate(cliente: Cliente) {
    if (cliente.id != null && cliente.id != '') {
      return this.http.put(`${API_BACKEND}/api/cliente`, cliente);
    } else {
      cliente.id = null;
      return this.http.post(`${API_BACKEND}/api/cliente`, cliente);
    }
  }

  findAll(page: number, count: number) {
    return this.http.get(`${API_BACKEND}/api/cliente/${page}/${count}`);
  }

  findByNome(page: number, count: number, nome: string) {
    return this.http.get(`${API_BACKEND}/api/cliente/${page}/${count}/nome/${nome}`);
  }

  findById(id: string) {
    return this.http.get(`${API_BACKEND}/api/cliente/${id}`);
  }

  delete(id: string) {
    return this.http.delete(`${API_BACKEND}/api/cliente/${id}`);
  }
}



