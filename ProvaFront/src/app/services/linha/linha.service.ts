import { HttpClient } from '@angular/common/http';
import { API_BACKEND } from './../provafront.api';
import { Linha } from './../../model/linha';
import { Injectable } from '@angular/core';

@Injectable()
export class LinhaService {

  constructor(private http: HttpClient) { }

  createOrUpdate(linha: Linha) {
    if (linha.id != null && linha.id != '') {
      return this.http.put(`${API_BACKEND}/api/linha`, linha);
    } else {
      linha.id = null;
      return this.http.post(`${API_BACKEND}/api/linha`, linha);
    }
  }

  findAll(page: number, count: number) {
    return this.http.get(`${API_BACKEND}/api/linha/${page}/${count}`);
  }

  findByTipo(page: number, count: number, tipo: string) {
    return this.http.get(`${API_BACKEND}/api/linha/${page}/${count}/${tipo}`);
  }

  findById(id: string) {
    return this.http.get(`${API_BACKEND}/api/linha/id/${id}`);
  }

  findByNome(page: number, count: number, tipo: string, nome: string) {
    return this.http.get(`${API_BACKEND}/api/linha/${page}/${count}/${tipo}/nome/${nome}`);
  } 


  findByCodigo(codigo: string) {
    return this.http.get(`${API_BACKEND}/api/linha/codigo/${codigo}`);
  }

  findItinerario(codigo: string) {
    return this.http.get(`${API_BACKEND}/api/linha/itinerarios/${codigo}`);
  }

  delete(id: string) {
    return this.http.delete(`${API_BACKEND}/api/linha/${id}`);
  }

}
