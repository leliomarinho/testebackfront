import { Router } from '@angular/router';
import { DialogService } from './../../dialog.service';
import { LinhaService } from './../../services/linha/linha.service';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { ResponseApi } from '../../model/response-api';

@Component({
  selector: 'app-linha-list',
  templateUrl: './linha-list.component.html'
})
export class LinhaListComponent implements OnInit {

  codigoLinha: string;
  listaLotacoes: [];
  listaOnibus: [];
  listaItinerarios: [];
  listaLinhas = [];
  nome: string;
  page: number = 0;
  count: number = 5;
  pages: Array<number>;
  shared: SharedService;
  message: {};
  classCss: {};
  tipoLinha: string;


  constructor(
    private dialogService: DialogService,
    private linhaService: LinhaService,
    private router: Router) {
    this.shared = SharedService.getInstance();
  }

  ngOnInit() {

    // this.findItinerarios(this.codigoLinha);

    // this.findAll(this.page, this.count);
  }
  
  findAllTipo(page: number, count: number) {
    this.linhaService.findByTipo(page, count, this.tipoLinha).subscribe((responseApi: ResponseApi) => {
      this.listaLinhas = responseApi['data']['content'];
      this.pages = new Array(responseApi['data']['totalPages']);
    }, err => {
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });

  }

  findByNome(): void {
    this.page = 0;
    this.count = 5;
    this.linhaService.findByNome(this.page, this.count, this.tipoLinha, this.nome)
      .subscribe((responseApi: ResponseApi) => {
        this.linhaService = responseApi['data']['content'];
        this.pages = new Array(responseApi['data']['totalPages']);
      }, err => {
        this.showMessage({
          type: 'error',
          text: err['error']['errors'][0]
        });
      });

  }

  setNextPage(event: any) {
    event.preventDefault();
    if (this.page + 1 < this.pages.length) {
      this.page = this.page + 1;
      this.findAllTipo(this.page, this.count);
    }
  }

  setPreviousPage(event: any) {
    event.preventDefault();
    if (this.page > 0) {
      this.page = this.page - 1;
      this.findAllTipo(this.page, this.count);
    }
  }

  setPage(i, event: any) {
    event.preventDefault();
    this.page = i;
    this.findAllTipo(this.page, this.count);
  }

  private showMessage(message: { type: string, text: string }): void {
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    }, 3000);
  }

  private buildClasses(type: string): void {
    this.classCss = {
      'alert': true
    }
    this.classCss['alert-' + type] = true;
  }


  findByLinhas(tipo: string): void {
    this.page = 0;
    this.count = 5;
    this.tipoLinha = tipo; 
    this.linhaService.findByTipo(this.page, this.count, tipo)
      .subscribe((responseApi: ResponseApi) => {
        this.listaLinhas = responseApi['data']['content'];
        this.pages = new Array(responseApi['data']['totalPages']);
      }, err => {
        this.showMessage({
          type: 'error',
          text: err['error']['errors'][0]
        });
      });

  }


  findItinerarios(codigoLinha: string) {
    this.linhaService.findItinerario(codigoLinha).subscribe((responseApi: ResponseApi) => {
      this.listaItinerarios = responseApi['data']['content'];
    }, err => {
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });

  }




}
