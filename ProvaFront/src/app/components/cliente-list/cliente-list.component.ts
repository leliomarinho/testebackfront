import { Router } from '@angular/router';
import { ClienteService } from './../../services/cliente/cliente.service';
import { DialogService } from './../../dialog.service';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { ResponseApi } from '../../model/response-api';

@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html'
})
export class ClienteListComponent implements OnInit {
  page: number = 0;
  count: number = 5;
  nome: string;
  IdLinha: string;
  pages: Array<number>;
  shared: SharedService;
  message: {};
  classCss: {};
  listaCliente = [];

  constructor(
    private dialogService: DialogService,
    private clienteService: ClienteService,
    private router: Router) {
    this.shared = SharedService.getInstance();
  }

  ngOnInit() {
    this.findAll(this.page, this.count);
  }

  findAll(page: number, count: number) {
    this.clienteService.findAll(page, count).subscribe((responseApi: ResponseApi) => {
      this.listaCliente = responseApi['data']['content'];
      this.pages = new Array(responseApi['data']['totalPages']);
    }, err => {
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });

  }

  findByNome(): void {
    this.page = 0;
    this.count = 5;
    this.clienteService.findByNome(this.page, this.count, this.nome)
      .subscribe((responseApi: ResponseApi) => {
        this.listaCliente = responseApi['data']['content'];
        this.pages = new Array(responseApi['data']['totalPages']);
      }, err => {
        this.showMessage({
          type: 'error',
          text: err['error']['errors'][0]
        });
      });

  }



  delete(id: string) {
    this.dialogService.confirm('Deseja excluir o cadastro ?')
      .then((candelete: boolean) => {
        if (candelete) {
          this.message = {};
          this.clienteService.delete(id).subscribe((responseApi: ResponseApi) => {
            this.showMessage({
              type: 'success',
              text: `Registro deletado`
            });
            this.findAll(this.page, this.count);
          }, err => {
            this.showMessage({
              type: 'error',
              text: err['error']['errors'][0]
            });
          });
        }
      });
  }

  setNextPage(event: any) {
    event.preventDefault();
    if (this.page + 1 < this.pages.length) {
      this.page = this.page + 1;
      this.findAll(this.page, this.count);
    }
  }

  setPreviousPage(event: any) {
    event.preventDefault();
    if (this.page > 0) {
      this.page = this.page - 1;
      this.findAll(this.page, this.count);
    }
  }

  setPage(i, event: any) {
    event.preventDefault();
    this.page = i;
    this.findAll(this.page, this.count);
  }

  private showMessage(message: { type: string, text: string }): void {
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    }, 3000);
  }

  private buildClasses(type: string): void {
    this.classCss = {
      'alert': true
    }
    this.classCss['alert-' + type] = true;
  }

}
