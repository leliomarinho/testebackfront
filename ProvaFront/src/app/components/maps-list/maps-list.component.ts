import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
// import { } from '@types/googlemaps';

@Component({
  selector: 'app-maps-list',
  templateUrl: './maps-list.component.html'
})
export class MapsListComponent implements OnInit {
 
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  latitude: any;
  longitude: any;
  isHidden = false;

  constructor() { }


  ngOnInit() {
  }

  LimparMapa() {

    let mapProp = {

      center: new google.maps.LatLng(-30.03155681213800000, -51.22697925584400000),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP

    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

  }

  IniciarMapa() {

    let mapProp = {

      center: new google.maps.LatLng(-30.03155681213800000, -51.22697925584400000),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP

    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

  }
  DesenharMapa() {


    var lat = [];
    var lng = [];
    lat[0] = ['-30.03155681213800000'];
    lng[0] = ['-51.22697925584400000'];
    lat[1] = ['-30.03154681213800000'];
    lng[1] = ['-51.22779525584400000'];
    lat[2] = ['-30.03075381213800000'];
    lng[2] = ['-51.22776425584400000'];
    lat[3] = ['-30.03077881213800000'];
    lng[3] = ['-51.22762825584400000'];
    lat[4] = ['-30.03083581213800000'];
    lng[4] = ['-51.22691325584400000'];
    lat[5] = ['-30.03090681213800000'];
    lng[5] = ['-51.22586725584400000'];
    lat[6] = ['-30.03097981213800000'];
    lng[6] = ['-51.22481125584400000'];
    lat[7] = ['-30.03102481213800000'];
    lng[7] = ['-51.22435225584400000'];
    lat[8] = ['-30.03106281213800000'];
    lng[8] = ['-51.22425625584400000'];
    lat[9] = ['-30.03112081213800000'];
    lng[9] = ['-51.22418925584400000'];
    lat[10] = ['-30.03146181213800000'];
    lng[10] = ['-51.22392425584400000'];
    lat[11] = ['-30.03191181213800000'];
    lng[11] = ['-51.22357725584400000'];
    lat[12] = ['-30.03218281213800000'];
    lng[12] = ['-51.22337725584400000'];
    lat[13] = ['-30.03249281213800000'];
    lng[13] = ['-51.22313525584400000'];
    lat[14] = ['-30.03251781213800000'];
    lng[14] = ['-51.22311625584400000'];
    lat[15] = ['-30.03346281213800000'];
    lng[15] = ['-51.22237825584400000'];
    lat[16] = ['-30.03354581213800000'];
    lng[16] = ['-51.22249925584400000'];
    lat[17] = ['-30.03411081213800000'];
    lng[17] = ['-51.22312025584400000'];
    lat[18] = ['-30.03418181213800000'];
    lng[18] = ['-51.22319425584400000'];
    lat[19] = ['-30.03461381213800000'];
    lng[19] = ['-51.22365225584400000'];
    lat[20] = ['-30.03466781213800000'];
    lng[20] = ['-51.22372125584400000'];
    lat[21] = ['-30.03463481213800000'];
    lng[21] = ['-51.22375925584400000'];


    // this.map.setCenter(new google.maps.LatLng(lat[0], lng[0]));

    // let marker
    // for (var i = 0; i < lat.length; i++) {
    //   //var dHs = dH[i];
    //   marker = new google.maps.Marker({
    //     position: { lat: parseFloat(lat[i]), lng: parseFloat(lng[i]) },
    //     // DrawingManager: { lat: parseFloat(lat[i]), lng: parseFloat(lng[i]) },
    //     map: this.map,
    //     //icon: this.iconBase + this.selectedMarkerType,
    //     title: 'nome da linha',
    //   });
    // }



    var IniciarMapa = {
      center: new google.maps.LatLng(lat[0], lng[0]),
      zoom: 15,
      MapTypeId: google.maps.MapTypeId.ROADMAP,
      title: "Rota 01"

    };

    var novoMapa = new google.maps.Map(this.gmapElement.nativeElement, IniciarMapa)

    var latLngBounds = new google.maps.LatLngBounds();

    var RotaDestino = [];

    for (var i = 0; i < lat.length; i++) {

      RotaDestino.push(new google.maps.LatLng(lat[i], lng[i]));

    }

    var VisualizacaoMapa = {
      path: RotaDestino,
      // strokeColor: "#ff0000",
      strokeColor: "#FF5563",
      strokeWeight: 4
    };

    var polyline = new google.maps.Polyline(VisualizacaoMapa);

    polyline.setMap(novoMapa);

  }

}
