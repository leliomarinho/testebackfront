import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SharedService } from '../../services/shared.service';
import { ClienteService } from '../../services/cliente/cliente.service';
import { ResponseApi } from '../../model/response-api';
import { Cliente } from '../../model/cliente';

@Component({
  selector: 'app-cliente-new',
  templateUrl: './cliente-new.component.html'
})
export class ClienteNewComponent implements OnInit {

  @ViewChild("form")
  form: NgForm;
  page: number = 0;
  count: number = 5;
  pages: Array<number>;
  cliente = new Cliente();
  shared: SharedService;
  message: {};
  classCss: {};
  

  constructor(
    private clienteService: ClienteService,
    private route: ActivatedRoute) {
    this.shared = SharedService.getInstance();
  }

  ngOnInit() {
    let id: string = this.route.snapshot.params['id'];
    if (id != undefined) {
      this.findById(id);
    }


  }


  findById(id: string) {
    this.clienteService.findById(id).subscribe((responseApi: ResponseApi) => {
      this.cliente = responseApi.data;
    }, err => {
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });
  }

  registrar() {
    this.message = {};
    this.clienteService.createOrUpdate(this.cliente).subscribe((responseApi: ResponseApi) => {
      this.cliente = new Cliente();
      let userRet: Cliente = responseApi.data;
      this.form.resetForm();
      this.showMessage({
        type: 'success',
        text: `Cadastro ${userRet.email} efetuado com sucesso`
      });
    }, err => {
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });
  }

  getFormGroupClass(isInvalid: boolean, isDirty: boolean): {} {
    return {
      'form-group': true,
      'has-error': isInvalid && isDirty,
      'has-success': !isInvalid && isDirty
    };
  }




  private showMessage(message: { type: string, text: string }): void {
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    }, 3000);
  }

  private buildClasses(type: string): void {
    this.classCss = {
      'alert': true
    }
    this.classCss['alert-' + type] = true;
  }

  

}
