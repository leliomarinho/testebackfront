import { Linha } from './linha';
export class Cliente {
        public id: string;
        public nome: string;
        public email: string;
        public cpf: string;
        public linha: Array<Linha>;
    }