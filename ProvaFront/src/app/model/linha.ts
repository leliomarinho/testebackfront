import { Itinerario } from "./itinerario";

export class Linha {
    public id: string;
    public codigoId: string;
    public codigo: string;
    public nome: string;
    public tipo: string;
    public itnerarios: Array<Itinerario>;
}




